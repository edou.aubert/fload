# !! Project under developpment (help is welcome here )!
# Fload

This project allows you to install your own file-sharing instance. It is a personal WeTransfer-like solution !

#Requirements

For this project, you will need the following programs :
- Node.js (and a few dependencies that will be installed during the Setup)
- Mysql (if you want to change the database server, you may easily change the script yourself to make it work. However, default there is mysql)

# Setup

**1.** First, you must setup your mysql database :

open terminal and connect to your mysql instance :

`sudo mysql -u root`

create the database and the table associated with it :

`CREATE DATABASE <your database name>`

`CREATE TABLE <your database name>.files ( Filename TEXT NOT NULL , Timestamp DATE NOT NULL , MimeType VARCHAR(256) NOT NULL , Extension VARCHAR(256) NOT NULL , ID INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (ID)) ENGINE = InnoDB;`

>if you want to create another user for your fload instance, you can do that. This user will be used for the main.js script :
>
> create user :
> 
> `CREATE USER '<username>'@'localhost' IDENTIFIED BY '<password>' ;`
> 
> grant privileges to user :
> 
> `GRANT ALL PRIVILEGES ON <your fload database name>.files ;`
> 
> save :
> 
> `FLUSH PRIVILEGES ;`
> 
> and finally exit :
> 
> `EXIT;`


**2.** Once you have cloned the project to your server, you must edit the file main.js. You need to set :

- The mysql database name
- The mysql user and password
- A passcode (you'll need it to access to your fload instance)
- The adress (can be IP adress or web domain, and MUST correspond to the server's location)
- The port (you can set it to port 80 for example, which is the default http port)

**3.** run `npm install` in the cloned directory (this will install the needed dependencies).

**4.** Start main.js 

(for linux users, if you want to run it automatically, you can set a cron job with the following commands :

`sudo -u www-data crontab -e`

Then, add the following line :

`@reboot cd /path/to/the/project && node main.js`

You may also use [pm2.io]('https://pm2.io') to make it more efficient to use as an automatic project)

**5.** You're set !

**NOTE : this project is still under developpment. Therefore, there might be a few bugs to install the project properly. If you get any problem, please report it to us.**
