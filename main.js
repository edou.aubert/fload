const express = require('express');
var mkdirp = require('mkdirp');
const ejs = require('ejs');
var app = express() ;
const Mime = require('mime');
const fs = require('fs');
var Busboy = require('busboy');
path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
const url = require('url');
const querystring = require('querystring');
var bodyParser = require('body-parser');
var http = require('http').Server(app);
const FileType = require('file-type');
var mysql      = require('mysql');
var MySQLStore = require('express-mysql-session')(session);

// IMPORTANT :
// FILL THE LINES BELOW
var databaseName = //Enter database name (you must create it yourself and make sure that the mysql user's got access to it)
var ipadress = //Enter your IP adress (or web domain). !Warning! : Must be string
var port = //Choose the port to use for your server. !Warning! : Must be int

var connection = mysql.createConnection({
  host     : 'localhost', //Enter mysql credentials
  user     : '', //mysql user
  password : '', //mysql password
  database : databaseName
});
//YOU ALSO HAVE TO SET THE PASSCODE
var passcode = //Set the Passcode
// That's all, you're set !


/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

app.use(cookieParser());

app.use(session({
    secret: "Shh, its a secret!",
    resave: true,
    saveUninitialized: true,
    maxAge: null,
}));
//                                                                      Powered by Edouda
//      ||========  ||             ====          ====       ||===\\
//      ||          ||           ||    ||       //  \\      ||    \\
//      ||====      ||          ||      ||     ||    ||     ||     \|
//      ||          ||          ||      ||    //======\\    ||     /|
//      ||          ||           ||    ||    ||        ||   ||    //
//      ||          ||========     ====     //          \\  ||===//

app.get('/fload', function(req,res) {
    if (req.session.floadSession==true) {
        Floadfiles=[]
        Floadtimestamp= []
        connection.query('SELECT * FROM files', function (error, results, fields) {
            if (typeof(results)!=='undefined' && typeof(results[0])!=='undefined') {
                for (var i=0; i<results.length;i++) {
                    Floadtimestamp.push(results[i].Timestamp);
                    Floadfiles.push(results[i].Filename)
                }
                console.log(Floadfiles)
                res.render('fload.ejs',{filename : Floadfiles, address : ipadress+':'+port})
            }
            else {
                res.render('fload.ejs',{filename : [], address : ipadress+':'+port})
            }
        });
    }
    else {
        res.render('floadPSSCD.ejs',{address : ipadress+':'+port})    
    }
});

app.get('/fload/file/:filename',function(req,res) {
    if (req.params.filename) {
        file=req.params.filename;
        var dirname = path.resolve(".")+'/uploads/';
        if (fs.existsSync(dirname + file)) {
            res.render('file.ejs', { directory : ipadress+':'+port+'/FloadFileSend/' + file, name : file, fileType : Mime.getType(file) })
        } else {
            res.status(404).render('error.ejs',{ info : 'The data of your link seems to be invalid.', tip: 'It probably expired. We recommend you to refresh your EACN2 webpage in order to get valid links.'})
        }
        tryIt = null;
    }
})
app.get('/FloadFileSend/:filename/',function(req,res){
    file=req.params.filename;
    var dirname = path.resolve(".")+'/uploads/';
    if (fs.existsSync(dirname + file)) {
        res.sendFile(dirname+file)
    }
    else {
        res.status(404).render('error.ejs',{ info : 'The data of your link seems to be invalid.', tip: 'It probably expired. We recommend you to refresh your EACN2 webpage in order to get valid links.'})
    }
})

app.post('/fload', function (req,res) {
    console.log(req.body)
    if (req.body.psswd==passcode) {
        req.session.floadSession = true;
        res.end('{"status" : "1"}')
    }
    else if (req.body.disconnect) {
        req.session.floadSession = null;
        res.redirect(ipadress+':'+port+'/fload')
    }
    else if (req.body.removeFiles) {
        docToRemove= path.resolve(".")+'/uploads/' + req.body.removeFiles
        console.log('Removing..')
        connection.query('DELETE FROM files WHERE Filename=\'' +mysql_real_escape_string(req.body.removeFiles)+'\'', function (error, results, fields) {
            if (error) throw error; return false;
        });
        if (docToRemove.includes('../') || docToRemove.includes('/..') || docToRemove.includes('~/')) {
            res.end('Error, forbidden');
            return false ;
        }
        // + Do a request to check that the user owns the Text document +
        fs.unlink(docToRemove, (err) => {
          if (err) {
            console.error(err)
            res.end('file does\'nt seem to exist !');
          }
          else {
              res.end('2');
          }
        })
    }
});

app.post('/fload/upload',function(req,res) {
    console.log('Fload : Storing data..')
    var busboy = new Busboy({ headers: req.headers });
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        docIdDir= path.resolve(".")+'/uploads/'
        var rfilename, rfileext;
        for (var i=filename.length;i>0;i--) {
            if (filename.substr(i-1,1)+''=='.') {
                rfilename = filename.substr(0,i-1)
                rfileext = filename.substr(i-1,filename.length-i+1)
                break;
            }
        }
        var n=0;
        mkdirp.sync(docIdDir)
        while (fs.existsSync(docIdDir + '/' + filename)) {
            filename = rfilename + '_(' + n + ')' + rfileext;
            n = n + 1 ;
        }
        var mimeType = Mime.getType(rfileext.substr(1, rfileext.length));
        if (rfileext == '.py' || rfileext == '.js' || rfileext=='.html' || rfileext=='.php' || rfileext=='.sql' || rfileext=='.css') {
            mimeType = 'Application/' + rfileext.substr(1,rfileext.length-1)
        }
        var saveTo = path.join(docIdDir + '/' + filename);
        (async () => {
            const stream = fs.createReadStream(docIdDir + '/' + filename);

            console.log(await FileType.fromStream(stream));
            //=> {ext: 'png', mime: 'image/png'}
        })();;
        file.pipe(fs.createWriteStream(saveTo))
        connection.query('INSERT INTO files (Filename, Extension, MimeType) VALUES  (\''+mysql_real_escape_string(filename)+'\',\''+mysql_real_escape_string(rfileext)+'\',\''+mysql_real_escape_string(mimeType)+'\')', function (error, results, fields) {
            if (error) throw error;
            else {}
        });
    });

    busboy.on('finish', function() {
        res.writeHead(200, { 'Connection': 'close' });
        res.end("200");
    });
        
    return req.pipe(busboy); 
})

app.listen(parseInt(port));// You can change the port here if you want to
